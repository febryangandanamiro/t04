class pesanan extends kue{
    private double Berat;

    public pesanan(String name, int price, int berat) {
        super(name, price);
        this.Berat = berat;
    }

    public double hitungHarga() {
        return super.getPrice() * Berat;
    }

    @Override
    public double Berat() {
        return Berat;
    }

    @Override
    public double Jumlah() {
      return 0;
    }
}

