class readystock extends kue{
    private double Jumlah;

    public readystock(String name, double price, double jumlah) {
        super(name, price);
        this.Jumlah = jumlah;
    }

    public double hitungHarga() {
        return super.getPrice() * Jumlah * 2;
    }

    @Override
    public double Berat() {
       return 0;
    }

    @Override
    public double Jumlah() {
       return Jumlah;
    }
}
